import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Boards from '../src/components/boards';
import BoardWithId from '../src/components/boardWithId';
import Notfound from './notFound';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';

const routing = (
    <Router>
        <Switch>
            <Route exact path = "/" component = {App} />
            <Route exact path = "/boards" component = {Boards} />
            <Route path = "/boards/:board_id" component = {BoardWithId}/>
            <Route component = {Notfound}/>
        </Switch>
    </Router>
)

ReactDOM.render(routing, document.getElementById("root"));