import React, { Component } from 'react';
import Header from '../src/components/header';
import Main from '../src/components/main';

class App extends Component {
    state = {}
    render() { 
        return (
            <div>
                <Header />
                <div className="d-flex justify-content-center">
                    <h1 className="text-secondary">Home Page</h1>
                </div>
                <Main />
            </div>
        );
    }
}
 
export default App;