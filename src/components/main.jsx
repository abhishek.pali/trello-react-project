import React, { Component } from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import Boards from './boards';
import "../../src/App.css";
class Main extends Component {
    state = {
        createBoardDiv: true,
        showNewBoardInputDiv: false,
        boardNameInput: "",
    }

    createNewBoard = () => {
        this.setState({
            createBoardDiv: false,
            showNewBoardInputDiv: true
        });
    }

    handleInputChange = (event) => {
        this.setState({
            boardNameInput: event.target.value
        });
    }

    addNewBoard = () => {

        const apiKey = "042ad0a4e07ee114cc5df4534c75a0bc";
        const token = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";

        fetch(`https://api.trello.com/1/boards/?name=${this.state.boardNameInput}&key=${apiKey}&token=${token}`, {
                    method: 'POST'
                    })
                    .then(response => {
                        console.log(
                            `Response: ${response.status} ${response.statusText}`
                    );
                        return response.text();
                    })
                    .then((text) => {
                        console.log(text);
                        window.location = "/boards";
                        this.setState({
                            createBoardDiv: true,
                            showNewBoardInputDiv: false
                        });
                    })
                    .catch((err) => {
                        console.error(err);
                    });
    }

    handleClose = () => {
        this.setState({
            createBoardDiv: true,
            showNewBoardInputDiv: false,
            boardNameInput: ""
        });
    }

    render() { 
        return (
            <div>
                <h5 className={this.state.createBoardDiv ? "m-4 create-board" : "d-none"} onClick={this.createNewBoard}>Create new board</h5>
                <div className={this.state.showNewBoardInputDiv ? "m-4" : "d-none"}>
                    <h5>Enter the name of new board that you want to create in the input box below :- </h5>
                    <input type="text" value={this.state.boardNameInput} onChange={this.handleInputChange}/>
                    <button type="button" className="btn btn-sm btn-success mr-1 ml-1" onClick={this.addNewBoard}>Create Board</button>
                    <button type="button" className="btn btn-sm btn-outline-secondary" onClick={this.handleClose}>X</button>
                </div>
                <ul>
                    <li>
                        <Link to="/boards">Boards</Link>
                    </li>
                </ul>
                <Switch>
                    <Route exact path = "/boards" component = {Boards} />
                </Switch>
            </div>
        );
    }
}
 
export default Main;