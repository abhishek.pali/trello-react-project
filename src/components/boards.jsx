import React, { Component } from 'react';
import Header from './header';
import { Switch, Route, Link } from 'react-router-dom';
import BoardWithId from './boardWithId';

class Boards extends Component {
    constructor(props) {
        super(props);
        this.state = {
            boardsError: null,
            isBoardsDataLoaded: false,
            boardsData: [],
        }
    }

    componentDidMount() {

        const apiKey = "042ad0a4e07ee114cc5df4534c75a0bc";
        const token = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";

        fetch(`https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${token}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json'
            }
            })
            .then(response => {
                console.log(
                `Response: ${response.status} ${response.statusText}`
                );
                return response.json();
            })
            .then(result => {
                // console.log(result);
                this.setState({
                  isBoardsDataLoaded: true,
                  boardsData: result
                });
              })
            .catch(err => {
                this.setState({
                  isBoardsDataLoaded: true,
                  boardsError: err
                });
              });
    }

    render() {
        const { boardsError, isBoardsDataLoaded, boardsData } = this.state;
        if (boardsError) 
            {
                return <div>Error: {boardsError.message}</div>;
            } 
        else if (!isBoardsDataLoaded) 
            {
                return <div>Loading...</div>;
            } 
        else 
            {
                return (
                        <div>
                            <Header />
                            <div className="m-2 font-weight-bold">
                                <h3>Boards List :</h3>
                                <ul>
                                    {boardsData.map(board => (
                                        <li key={board.id}>
                                            <Link to={`/boards/${board.id}`}>{board.name}</Link>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                            <Switch>
                                <Route path = "/boards/:board_id" component = {BoardWithId}/>
                            </Switch> 
                        </div>
                );
            }
        }
} 
export default Boards;