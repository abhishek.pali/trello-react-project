import React, { Component } from 'react';
import Card from './card'
import "../../src/App.css";
import Modal from 'react-modal';
class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cardsError: null,
            isCardsDataLoaded: false,
            cardsData: [],
            showAddCardDiv: false,
            showNewCardDiv: false,
            showAddAnotherCardDiv: true,
            isModalOpen: false,
            inputValue: "",
            textAreaValue: "",
            editedCard: false,
            targetId: "",
            hideOnDelete: false,
            checklistInput: "Checklist",
            showChecklistInputDiv: false,
            showChecklistDiv: false,
            showChecklistName: true,
            checklistName: "",
            editedChecklistName: false,
            showChecklistNameEditBox: false,
            checklistId: "",
            hideChecklist: false,
        }
    }

    static getDerivedStateFromProps(props, state) {
        if (state.idValue === undefined)
            {
                return {idValue: props.idValue};
            }
        else
            {
                return state;
            }
    }

    fetchAllCards = async() => {
        
        const apiKey = "042ad0a4e07ee114cc5df4534c75a0bc";
        const token = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";

        await fetch(`https://api.trello.com/1/lists/${this.state.idValue}/cards?key=${apiKey}&token=${token}`, {
                      method: 'GET',
                      headers: {
                          'Accept': 'application/json'
                      }
                      })
                      .then(response => {
                          console.log(
                          `Response: ${response.status} ${response.statusText}`
                          );
                          return response.json();
                      })
                      .then(result => {
                          this.setState({
                              isCardsDataLoaded: true,
                              cardsData: result,
                          });
                      })
                      .catch(err => {
                              this.setState({
                                isCardsDataLoaded: true,
                                cardsError: err
                              });
                            });
    }
   
    componentDidMount() {
        this.fetchAllCards();
    }

    addAnotherCard = () => {
        this.setState({
            showAddCardDiv: true,
            showAddAnotherCardDiv: false
        });
    }
    
    addCard = (e) => {

        const apiKey = "042ad0a4e07ee114cc5df4534c75a0bc";
        const token = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";

        if (this.state.inputValue !== "")
            {
                fetch(`https://api.trello.com/1/cards?idList=${e.target.id}&name=${this.state.inputValue}&key=${apiKey}&token=${token}`, {
                    method: 'POST'
                    })
                    .then(response => {
                    console.log(
                        `Response: ${response.status} ${response.statusText}`
                    );
                    return response.text();
                    })
                    .then((text) => {
                        window.location.reload();
                        this.setState({
                            showAddCardDiv: false,
                            showAddAnotherCardDiv: true,
                            showNewCardDiv: true
                        });
                        
                    })
                    .catch((err) => {
                    console.error(err);
                    });   
            }
    }

    crossButtonHandler = () => {
        this.setState({
            showAddCardDiv: false,
            showAddAnotherCardDiv: true
        });
    }

    handleInputChange= (event) => {
        this.setState({
            inputValue: event.target.value
        });
    }

    handleModal = (e) => {
        // console.log(e.target);
        // console.log(e.target.innerText);
        this.setState({
            isModalOpen: true,
            targetValue: e.target.innerText,
            targetId: e.target.id,
            textAreaValue: e.target.innerText
        });

        const apiKey = "042ad0a4e07ee114cc5df4534c75a0bc";
        const token = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";

        fetch(`https://api.trello.com/1/cards/${e.target.id}/checklists?key=${apiKey}&token=${token}`, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json'
                }
                })
                .then(function(response) {                       
                    console.log(
                        `Response: ${response.status} ${response.statusText}`
                );
                    return response.json();
                })
                .then((result) => {
                    console.log(result);
                    this.setState({
                        checklistData: result
                    });
                })
                .catch((err) => {
                  console.error(err);
                });  
    }

    handleClose = () => {
        this.setState({
            isModalOpen: false,
            showChecklistInputDiv: false,
            showChecklistNameEditBox: false,
            showChecklistName: true
        });
    }

    handleEditingCard = (event) => {
        // console.log(e.target.id)
        this.setState({
            textAreaValue: event.target.value
        })
    }

    saveChange = () => {

        const apiKey = "042ad0a4e07ee114cc5df4534c75a0bc";
        const token = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";

        fetch(`https://api.trello.com/1/cards/${this.state.targetId}?name=${this.state.textAreaValue}&key=${apiKey}&token=${token}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json'
                }
                })
                .then(function(response) {                       
                    console.log(
                        `Response: ${response.status} ${response.statusText}`
                );
                    return response.text();
                })
                .then((text) => {
                    this.setState({
                        editedCard: true,
                        isModalOpen: false
                    });
                })
                .catch((err) => {
                  console.error(err);
                });    
    }

    handleDelete = () => {

        const apiKey = "042ad0a4e07ee114cc5df4534c75a0bc";
        const token = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";

        fetch(`https://api.trello.com/1/cards/${this.state.targetId}?key=${apiKey}&token=${token}`, {
                          method: 'DELETE'
                          })
                          .then(response => {
                            console.log(
                              `Response: ${response.status} ${response.statusText}`
                            );
                            return response.text();
                          })
                          .then((text) => {
                            this.setState({
                                isModalOpen: false,
                                hideOnDelete: true
                            });
                          })
                          .catch((err) => {
                            console.error(err);
                          }); 
    }

    handleChecklist = () => {
        this.setState({
            showChecklistInputDiv: true
        });
    }

    handleChecklistInput = (event) => {
        this.setState({
            checklistInput: event.target.value
        });
    }

    addChecklist = () => {

        const apiKey = "042ad0a4e07ee114cc5df4534c75a0bc";
        const token = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";

        fetch(`https://api.trello.com/1/checklists?idCard=${this.state.targetId}&name=
                ${this.state.checklistInput}&key=${apiKey}&token=${token}`, {
                method: 'POST'
                })
                .then(response => {
                    console.log(
                        `Response: ${response.status} ${response.statusText}`
                    );
                        return response.text();
                })
                .then((text) => {
                    window.location.reload();
                    this.setState({
                        showChecklistDiv: true,
                        showChecklistInputDiv: false
                    });
                })
                .catch((err) => {
                    console.error(err);
                }); 
    }

    handleInputClose = () => {
        this.setState({
            showChecklistInputDiv: false
        });
    }
    
    handleChecklistName = (event) => {
        console.log(event.target.id);
        console.log(event.target.innerText);
        this.setState({
            checklistId: event.target.id,
            checklistName: event.target.innerText,
            showChecklistName: false,
            showChecklistNameEditBox: true
        });
    }

    editChecklistName = (event) => {
        this.setState({
            checklistName: event.target.value
        });
    }

    saveChecklistName = () => {

        const apiKey = "042ad0a4e07ee114cc5df4534c75a0bc";
        const token = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";

        fetch(`https://api.trello.com/1/checklists/${this.state.checklistId}?name=${this.state.checklistName}&key=${apiKey}&token=${token}`, {
                method: 'PUT'
                })
                .then(response => {
                    console.log(
                        `Response: ${response.status} ${response.statusText}`
                    );
                        return response.text();
                })
                .then((text) => {
                    this.setState({
                        showChecklistName: true,
                        showChecklistNameEditBox: false,
                        editedChecklistName: true
                    });
                })
                .catch((err) => {
                    console.error(err);
                }); 
        
    }

    closeChecklistNameEditing = () => {
        this.setState({
            showChecklistName: true,
            showChecklistNameEditBox: false
        });
    }

    handleDeleteChecklist = (e) => {

        this.setState({
            checklistId: e.target.id
        });

        const apiKey = "042ad0a4e07ee114cc5df4534c75a0bc";
        const token = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";

        fetch(`https://api.trello.com/1/checklists/${e.target.id}?key=${apiKey}&token=${token}`, {
                method: 'DELETE'
                })
                .then(response => {
                    console.log(
                        `Response: ${response.status} ${response.statusText}`
                    );
                        return response.text();
                })
                .then((text) => {
                    window.location.reload();
                    this.setState({
                        hideChecklist: true
                    });
                })
                .catch((err) => {
                    console.error(err);
                }); 
    }

    render() {
        // console.log(this.state.checklistData[0] ? this.state.checklistData[0] : "hi");
        var cardValues = {
            cardsError: this.state.cardsError,
            isCardsDataLoaded: this.state.isCardsDataLoaded,
            cardsData: this.state.cardsData,
        }
        var thisCard = this.state.cardsData;
        // var checklists = this.state.checklistData;
        // console.log(checklists);
        
        if (this.props.listValues.boardError) 
            {
                return <div>Error: {this.props.listValues.boardError.message}</div>;
            } 
        else if (!this.props.listValues.isBoardDataLoaded) 
            {
                return <div>Loading...</div>;
            } 
        else 
            {
                return (
                        <div>
                            {thisCard ? thisCard.map(card => 
                                <div className={"card m-1 p-1 list-cards " + (this.state.hideOnDelete ? "d-none" : "")} 
                                    style={{width: "10rem"}} onClick={this.handleModal}
                                        id ={card.id}>
                                    <Card key = {card.id} card = {card} cardName = {this.state.editedCard ? this.state.textAreaValue : card.name} 
                                            cardValues = {cardValues} />
                                </div>
                            ): null}
                            <div className={"card m-1 p-1 " + (this.state.showNewCardDiv ? "" : "d-none")} style={{width: "10rem"}}>
                                <p>{this.state.inputValue}</p>
                            </div>
                            <div className={"card m-1 p-1 " + (this.state.showAddAnotherCardDiv ? "" : "d-none")} style={{width: "10rem"}}>
                                <button type="button" className="btn btn-outline-secondary another-card-button" onClick={this.addAnotherCard}>
                                    <span className="another-card-span">+ Add Another Card</span>
                                </button>
                            </div>
                            <div className={"card m-1 p-1 " + (this.state.showAddCardDiv ? "" : "d-none")} style={{width: "10rem"}}>
                                <input type="text" value={this.state.inputValue} onChange={this.handleInputChange}/>
                                <span className="m-1">
                                    <button type="button" className="mr-1 btn btn-sm btn-success add-card-button" 
                                            id = {this.props.idValue} onClick={this.addCard}>
                                        Add Card
                                    </button>
                                    <button type="button" className="btn btn-sm cross-button" onClick={this.crossButtonHandler}>
                                        X
                                    </button>
                                </span>
                            </div>
                            <Modal isOpen={this.state.isModalOpen} onRequestClose={this.handleClose}>
                                <div>
                                    <h3>{this.state.editedCard ? this.state.textAreaValue : this.state.targetValue}</h3>
                                    <p>Edit the name of the card in the textarea below : </p>
                                    <textarea value={this.state.textAreaValue} className="modal-textarea m-1" rows="10" cols="50" 
                                                onChange={this.handleEditingCard}/>
                                    <div>
                                        <button onClick={this.handleClose} className="m-1">Close</button>
                                        <button className ="m-1 save-changes-button" onClick={this.saveChange}>Save Changes</button>
                                        <button className="m-1 delete-card-button" onClick={this.handleDelete}>Delete Card</button>
                                        <button type="button" className="btn btn-success btn-sm m-1" onClick={this.handleChecklist}>
                                            Add Checklist
                                        </button>
                                    </div>
                                    {this.state.checklistData ? this.state.checklistData.map(checkList => 
                                        <div className={"ml-1 mt-3 checklist-div " + 
                                                        (this.state.hideChecklist && this.state.checklistId === checkList.id ? "d-none" : "")} 
                                             id={checkList.id}>
                                            <h5 className={(this.state.showChecklistName || this.state.checklistId !== checkList.id)
                                                            ? "checklist-name" : "d-none"} 
                                                onClick={this.handleChecklistName} id={checkList.id}>
                                                {(this.state.editedChecklistName && this.state.checklistId === checkList.id) 
                                                    ? this.state.checklistName : checkList.name}
                                            </h5>
                                            <div className={(this.state.showChecklistNameEditBox && this.state.checklistId === checkList.id)
                                                            ? "" : "d-none"}>
                                                <input type="text" value={this.state.checklistName} onChange={this.editChecklistName}/>
                                                <button type="button" className="btn btn-sm btn-success ml-1" onClick={this.saveChecklistName}>
                                                    Save
                                                </button>
                                                <button type="button" className="btn btn-sm btn-outline-secondary ml-1" 
                                                        onClick={this.closeChecklistNameEditing}>
                                                    X
                                                </button>
                                            </div>
                                            <div className="m-1">
                                                {checkList.checkItems.map(item => 
                                                    <div>
                                                        <input type="checkbox" id={item.name} name = {item.name}/>
                                                        <label htmlFor={item.name} className="m-1">{item.name}</label>
                                                    </div>
                                                )}
                                            </div>
                                            <button type="button" className="btn btn-outline-secondary btn-sm">Add an item</button>
                                            <button type="button" className="btn btn-danger btn-sm ml-1" onClick={this.handleDeleteChecklist}
                                                    id={checkList.id}>
                                                Delete {(this.state.editedChecklistName && this.state.checklistId === checkList.id) 
                                                    ? this.state.checklistName : checkList.name}
                                            </button>
                                        </div>
                                    ) : null}
                                    <div className={this.state.showChecklistInputDiv ? "m-1" : "d-none"}>
                                        <input type="text" value={this.state.checklistInput} onChange={this.handleChecklistInput}/>
                                        <button type="button" className="btn btn-success btn-sm m-1" onClick={this.addChecklist}>
                                            Add
                                        </button>
                                        <button type="button" className="btn btn-outline-secondary btn-sm m-1" onClick={this.handleInputClose}>
                                            X
                                        </button>
                                    </div>
                                    <div className={this.state.showChecklistDiv ? "m-1" : "d-none"}>
                                        <h5>{this.state.checklistInput}</h5>
                                        <button type="button" className="btn btn-outline-secondary btn-sm">Add an item</button>
                                        <button type="button" className="btn btn-danger btn-sm ml-1" onClick={this.handleDeleteChecklist}>
                                            Delete {this.state.checklistInput}
                                        </button>
                                    </div>
                                </div>
                            </Modal>
                        </div>                         
                );
            }
    }
}
 
export default List;