import React, { Component } from 'react';
import trello from '../images/trello-logo-blue.png';

class Header extends Component {
    state = {}
    render() { 
        return (
            <nav className="navbar navbar-dark bg-dark d-flex justify-content-center">
                <img src={trello} alt="trello" style={{height: "2em", width: "6em"}}/>
            </nav>
        );
    }
}
 
export default Header;