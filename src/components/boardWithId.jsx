import React, { Component } from 'react';
import Header from './header';
import "../../src/App.css";
import List from './list';
import Modal from 'react-modal';
class BoardWithId extends Component {
    constructor(props) {
        super(props);
        this.state = {
            boardError: null,
            isBoardDataLoaded: false,
            boardData: [],
            lists: [],
            boardName: "",
            boardId: "",
            showListDiv: false,
            showAddListButton: true,
            showInputDiv: false,
            inputValue: "",
            isModalOpen: false,
            textAreaValue: "",
            targetId: "",
            editedList: false,
            hideOnDelete: false,
            showBoardEditDiv: false,
            showBoardNameDiv: true,
            boardNameInput: "",
            isBoardNameEdited: false
        }
    }

    componentDidMount = async() => {

        const apiKey = "042ad0a4e07ee114cc5df4534c75a0bc";
        const token = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";

        await fetch(`https://api.trello.com/1/boards/${this.props.match.params.board_id}?lists=open&key=${apiKey}&token=${token}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json'
            }
            })
            .then(response => {
                console.log(
                `Response: ${response.status} ${response.statusText}`
                );
                return response.json();
            })
            .then(result => {
                this.setState({
                  isBoardDataLoaded: true,
                  boardData: result,
                  lists: result.lists,
                  boardName: result.name,
                  boardId: result.id
                });
            })
            .catch(err => {
                this.setState({
                  isBoardDataLoaded: true,
                  boardError: err
                });
              });
    }

    addAnotherList = () => {
        this.setState({
            showAddListButton: false,
            showInputDiv: true
        });
    }

    handleInput = (e) => {
        this.setState({
            inputValue: e.target.value
        });
    }

    addList = () => {

        const apiKey = "042ad0a4e07ee114cc5df4534c75a0bc";
        const token = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";

        if (this.state.inputValue !== "")
            {
                fetch(`https://api.trello.com/1/lists?name=${this.state.inputValue}&idBoard=
                        ${this.state.boardId}&pos=bottom&key=${apiKey}&token=${token}`, {
                        method: 'POST'
                        })
                        .then(response => {
                            console.log(
                            `Response: ${response.status} ${response.statusText}`
                            );
                            return response.text();
                        })
                        .then(text => {
                            window.location.reload();
                            this.setState({
                                showInputDiv: false,
                                showListDiv: true,
                                showAddListButton: true
                            });
                        })
                        .catch(err => {
                            console.error(err)
                        });
            }
    }

    crossButtonHandler = () => {
        this.setState({
            showAddListButton: true,
            showInputDiv: false
        })
    }

    handleModal = (e) => {
        console.log(e.target.id)
        this.setState({
            isModalOpen: true,
            targetId: e.target.id,
            textAreaValue: e.target.innerText
        })
    }

    handleClose = () => {
        this.setState({
            isModalOpen: false
        })
    }

    handleEditingList = (event) => {
        this.setState({
            textAreaValue: event.target.value
        })
    }

    saveChange = () => {

        const apiKey = "042ad0a4e07ee114cc5df4534c75a0bc";
        const token = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";

        fetch(`https://api.trello.com/1/lists/${this.state.targetId}?name=${this.state.textAreaValue}&key=${apiKey}&token=${token}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json'
                }
                })
                .then(function(response) {                       
                    console.log(
                        `Response: ${response.status} ${response.statusText}`
                );
                    return response.text();
                })
                .then((text) => {
                    this.setState({
                        editedList: true,
                        isModalOpen: false
                    });
                })
                .catch((err) => {
                  console.error(err);
                });    
    }

    handleDelete = () => {

        const apiKey = "042ad0a4e07ee114cc5df4534c75a0bc";
        const token = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";

        fetch(`https://api.trello.com/1/lists/${this.state.targetId}/closed?value=true&key=${apiKey}&token=${token}`, {
                    method: 'PUT'
                    })
                    .then(response => {
                        console.log(
                            `Response: ${response.status} ${response.statusText}`
                    );
                        return response.text();
                    })
                    .then((text) => {
                        this.setState({
                            isModalOpen: false,
                            hideOnDelete: true
                        });
                    })
                    .catch((err) => {
                            console.error(err);
                    }); 
    }

    handleBoardName = () => {
        this.setState({
            showBoardEditDiv: true,
            showBoardNameDiv: false,
            boardNameInput: this.state.boardName,
            isBoardNameEdited: true
        });
    }

    closeEditDiv = () => {
        this.setState({
            showBoardEditDiv: false,
            showBoardNameDiv: true
        });
    }

    handleBoardNameChange = (event) => {
        this.setState({
            boardNameInput: event.target.value
        });
    }

    saveBoardName = () => {
        
        const apiKey = "042ad0a4e07ee114cc5df4534c75a0bc";
        const token = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";

        fetch(`https://api.trello.com/1/boards/${this.state.boardId}?name=${this.state.boardNameInput}&key=${apiKey}&token=${token}`, {
                    method: 'PUT'
                    })
                    .then(response => {
                        console.log(
                            `Response: ${response.status} ${response.statusText}`
                    );
                        return response.text();
                    })
                    .then((text) => {
                        window.location.reload();
                        this.setState({
                            showBoardEditDiv: false,
                            showBoardNameDiv: true
                        });
                    })
                    .catch((err) => {
                        console.error(err);
                    }); 
    }

    handleDeleteBoard = () => {

        const apiKey = "042ad0a4e07ee114cc5df4534c75a0bc";
        const token = "012605bd179953705efb552189fea2673036a572433f73a94fcc927ffe23ec3f";

        fetch(`https://api.trello.com/1/boards/${this.state.boardId}?key=${apiKey}&token=${token}`, {
                    method: 'DELETE'
                    })
                    .then(response => {
                        console.log(
                            `Response: ${response.status} ${response.statusText}`
                    );
                        return response.text();
                    })
                    .then((text) => {
                        console.log(text);
                        window.location = "/";
                    })
                    .catch((err) => {
                        console.error(err);
                    });
    }

    render() {
        
        var listValues = {
            boardError: this.state.boardError,
            isBoardDataLoaded: this.state.isBoardDataLoaded,
            boardData: this.state.boardData,
            lists: this.state.lists
        }

        return (
            <div>
                <Header />
                <div className="">
                    <div className={this.state.showBoardEditDiv ? "m-2" : "d-none"}>
                        <input type="text" value={this.state.boardNameInput} onChange={this.handleBoardNameChange}/>
                        <span className="m-1">
                            <button type="button" onClick={this.saveBoardName} className="btn btn-sm btn-success mr-1">Save</button>
                            <button type="button" onClick={this.closeEditDiv} className="btn btn-sm btn-outline-secondary">X</button>
                        </span>              
                    </div>
                    <div className={this.state.showBoardNameDiv ? "" : "d-none"}>
                        <h5 className="m-2 font-weight-bold board-name" onClick={this.handleBoardName}>
                            {this.state.isBoardNameEdited ? this.state.boardNameInput : this.state.boardName}
                        </h5>
                        <span>
                            <button type="button" className="btn btn-sm btn-danger m-2" onClick={this.handleDeleteBoard}>
                                Delete Board
                            </button>
                        </span>
                    </div>
                    <div className="d-flex flex-row" id={this.state.boardData.id}>
                        {this.state.lists.length ? this.state.lists.map(list => 
                            <div className={list.id === this.state.targetId && this.state.hideOnDelete ? "d-none" : "list-card d-flex flex-column card m-2"} 
                                    style={{width: "16rem"}}>
                                <div className="m-2 font-weight-bold list-name-div" onClick={this.handleModal} id={list.id}>
                                    {list.id === this.state.targetId ? this.state.textAreaValue : list.name}
                                </div>
                                <List idValue = {list.id} list = {list} listValues = {listValues} />                   
                            </div>               
                        ): null}
                        <div className={"m-2 " + (this.state.showListDiv ? "list-card d-flex flex-column card" : "d-none")} style={{width: "16rem"}}>
                            <div className="m-2 font-weight-bold list-name-div">
                                {this.state.inputValue}
                            </div>
                            <div className="card m-1 p-1" style={{width: "15rem"}}>
                                <button type="button" className="btn btn-outline-secondary another-card-button">
                                    <span className="another-card-span">+ Add Another Card</span>
                                </button>
                            </div>
                        </div>
                        <div className="list-card d-flex flex-column card m-2" style={{width: "16rem"}}>
                            <div className="m-2 font-weight-bold">
                                <button type="button" className={"btn btn-sm btn-outline-secondary " + (this.state.showAddListButton ? "" : "d-none")}
                                        onClick={this.addAnotherList}>
                                    + Add another list
                                </button>
                                <div className={this.state.showInputDiv ? "m-1" : "d-none"}>
                                    <input type="text" value={this.state.inputValue} onChange={this.handleInput} className="add-list-input"/>
                                    <span>
                                        <button type="button" className="btn btn-sm btn-success m-1" onClick={this.addList}>Add list</button>
                                        <button type="button" className="btn btn-sm cross-button" onClick={this.crossButtonHandler}>
                                            X
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <Modal isOpen={this.state.isModalOpen} onRequestClose={this.handleClose}>
                            <h2>Edit the name of the list in the textarea below : </h2>
                            <textarea value={this.state.textAreaValue} className="modal-textarea m-1" rows="10" cols="50" 
                                    onChange={this.handleEditingList}/>
                            <div>
                                <button onClick={this.handleClose} className="m-1">Close</button>
                                <button className ="m-1 save-changes-button" onClick={this.saveChange}>Save Changes</button>
                                <button className="m-1 delete-card-button" onClick={this.handleDelete}>Delete List</button>
                            </div>
                        </Modal>    
                    </div>
                </div>
            </div>
        );
    }
}
 
export default BoardWithId;