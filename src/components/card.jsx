import React, { Component } from 'react';

class Card extends Component {

    render() { 
        
        if (this.props.cardValues.cardsError) 
            {
                return <div>Error: {this.props.cardValues.cardsError.message}</div>;
            } 
        else if (!this.props.cardValues.isCardsDataLoaded) 
            {
                return <div>Loading...</div>;
            } 
        else 
            {
                return (
                    <p id={this.props.card.id}>{this.props.cardName}</p>
                );
            }
    }
}
 
export default Card;